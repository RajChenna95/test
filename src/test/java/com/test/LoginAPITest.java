package com.test;

import java.io.IOException;
import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.request.APIRequest;
import com.request.HeaderMaps;

import io.restassured.http.Header;
import io.restassured.response.Response;

public class LoginAPITest {
	APIRequest apiRequest;
	

	@BeforeMethod
	public void setup() throws IOException {
	//	apiRequest = new APIRequest();
		apiRequest.generateAPIURI();
		apiRequest.generateAPIHeader();
		apiRequest.generateAPIBody();
	}

	@Test
	public void loginTest() {
		Response r = apiRequest.getResponse(apiRequest.getLOGIN_ENDPOINT(), "POST");
		Assert.assertEquals(apiRequest.getResponseStatusCode(r), 200);
	}

}
