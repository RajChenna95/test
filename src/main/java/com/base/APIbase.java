package com.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.codec.binary.Base32;

public class APIbase {
	private String BASEURL;
	private String USERNAME;
	private String PASSWORD;

	public APIbase() throws IOException {

		super();

		FileInputStream input1 = new FileInputStream(
				"C:\\Users\\jatin\\Google Drive\\Test Automation Academy\\JAVA\\API-Automation-Framework\\src\\main\\java\\com\\config\\config.properties");
		Properties prop = new Properties();
		prop.load(input1);
		System.out.println(prop.getProperty("BASEURL"));
		System.out.println(prop.getProperty("USERNAME"));
		System.out.println(prop.getProperty("PASSWORD"));
		System.out.println(prop.getProperty("LOGIN_ENDPOINT"));
	}

	public String getBASEURL() {
		return BASEURL;
	}

	public String getUSERNAME() {
		return USERNAME;
	}

	public String getPASSWORD() {
		return PASSWORD;
	}

}
