package com.request;

public class HeaderMaps {

	public String keys;
	public String values;

	public HeaderMaps(String keys, String values) {
		super();
		this.keys = keys;
		this.values = values;
	}

	public String getKeys() {
		return keys;
	}

	public String getValues() {
		return values;
	}

}
