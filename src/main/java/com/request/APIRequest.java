package com.request;

import java.io.IOException;
import java.util.HashMap;

import org.json.simple.JSONObject;

import com.base.APIbase;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class APIRequest extends APIbase {
	
	
	public APIRequest() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	private RequestSpecification request;

	

	public void generateAPIURI() {
		RestAssured.baseURI = getBASEURL();
		request = RestAssured.given();

	}

	public void generateAPIHeader() {

		request.header("Content-Type", "application/json");
	}

	public void generateAPIBody() {
		JSONObject requestParams = new JSONObject();
		requestParams.put("email", getUSERNAME());
		requestParams.put("password", getPASSWORD());
		request.body(requestParams.toString());
	}

	public Response getResponse(String endPoint, String action) {

		Response response = null;
		if (action.equalsIgnoreCase("get")) {
			response = request.get(endPoint);
		} else if (action.equalsIgnoreCase("put")) {
			response = request.put(endPoint);
		} else if (action.equalsIgnoreCase("post")) {
			response = request.post(endPoint);
		}

		else if (action.equalsIgnoreCase("delete")) {
			response = request.delete(endPoint);
		}

		else {
			System.out.println("Wrong Input");
		}

		return response;

	}

	public String getResponseBody(Response response) {
		return response.getBody().asString();

	}

	public int getResponseStatusCode(Response response) {
		return response.getStatusCode();

	}

}
